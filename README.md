# 600 Watt Logos und Design Guide

## Typografie

Die im Logo verwendete Schrift ist [Yomogi](https://fonts.google.com/specimen/Yomogi).

Zusätzlich wird [Heebo](https://fonts.google.com/specimen/Heebo) verwendet.
